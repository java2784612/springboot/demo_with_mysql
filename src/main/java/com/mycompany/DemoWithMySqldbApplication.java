package com.mycompany;

import com.mycompany.entity.Blog;
import com.mycompany.repository.BlogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class DemoWithMySqldbApplication implements CommandLineRunner {

    @Autowired
    private BlogRepository blogRepository;

    public static void main(String[] args) { SpringApplication.run(DemoWithMySqldbApplication.class, args); }

    @Override
    public void run(String... args) throws Exception {

        // Create 2 blogs examples
        Blog b1 = new Blog("Blog Exemple 1", "1111  11   1 11111 1 1 1111 11111 1111111111 111 11 1111");
        Blog b2 = new Blog("Blog Exemple 2", "222 22 22222 22 22222 22 2 22 22 222 2222222 2 22 222222");
        Blog b3 = new Blog("Blog Exemple 3", "Exemple de contenu.");

        System.out.println("b1 avant enreg : " + b1);
        System.out.println("b2 avant enreg : " + b2);
        System.out.println("b3 avant enreg : " + b3);

        // Save() in DB
        blogRepository.save(b1);
        blogRepository.save(b2);
        blogRepository.save(b3);

        // FindAll() Example
        List<Blog> blogB = blogRepository.findAll();
        System.out.println("Blog(s) en base :");
        blogB.forEach(blog -> System.out.println(blog.getTitre() + " -  Contenu : " + blog.getContenu()));

    }
}
