package com.mycompany.repository;

import com.mycompany.entity.Blog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BlogRepository extends JpaRepository<Blog, Long> {

    @Query("From Blog b")
    List<Blog> trouverTous();

}
