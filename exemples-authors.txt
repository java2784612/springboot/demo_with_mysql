



Authors a1 = new Authors("Jean", "DUPONT", "26", "12 rue de la Paix", "69001", "Lyon");
Authors a2 = new Authors("Marie", "LECLERC", "31", "8 rue des Champs", "35000", "Rennes");
Authors a3 = new Authors("Sophie", "BERTRAND", "22", "16 rue de la Gare", "59000", "Lille");
Authors a4 = new Authors("Luc", "GARCIA", "39", "25 avenue des Roses", "13000", "Marseille");
Authors a5 = new Authors("Julie", "MOREAU", "28", "42 rue du Commerce", "44000", "Nantes");
Authors a6 = new Authors("Thomas", "ROBERT", "34", "10 avenue du Président", "75008", "Paris");
Authors a7 = new Authors("Caroline", "MARTIN", "29", "3 impasse des Mimosas", "34000", "Montpellier");
Authors a8 = new Authors("Maxime", "LEROY", "47", "6 rue de la Liberté", "69002", "Lyon");
Authors a9 = new Authors("Alexandra", "DUBOIS", "25", "7 avenue de la République", "92000", "Nanterre");
Authors a10 = new Authors("Pierre", "FONTAINE", "41", "14 avenue des Acacias", "31000", "Toulouse");
Authors a11 = new Authors("Laurent", "GIRARD", "36", "20 rue de la Roquette", "75011", "Paris");
Authors a12 = new Authors("Emilie", "PEREZ", "27", "18 rue des Fleurs", "13005", "Marseille");
Authors a13 = new Authors("Nicolas", "MARTINEZ", "30", "9 rue de la République", "06000", "Nice");
Authors a14 = new Authors("Céline", "ROUSSEAU", "33", "5 rue des Cerisiers", "69005", "Lyon");
Authors a15 = new Authors("François", "MOREL", "44", "21 rue des Lilas", "75020", "Paris");
Authors a16 = new Authors("Sandrine", "LEFEBVRE", "32", "11 rue du Moulin", "59000", "Lille");
Authors a17 = new Authors("Benoît", "MARTIN", "38", "17 avenue de la Paix", "69006", "Lyon");
Authors a18 = new Authors("Elodie", "GARNIER", "23", "2 rue de la Fontaine", "44000", "Nantes");
Authors a19 = new Authors("Thierry", "LEMAIRE", "48", "4 avenue des Champs-Élysées", "75008", "Paris");
Authors a20 = new Authors("Anne", "BERGER", "37", "15 rue de la Victoire", "75009", "Paris");
Authors a21 = new Authors("Sébastien", "DUMAS", "35", "19 avenue des Roses", "13000", "Marseille");